// Циклы необходимы программисту для многократного выполнения одного и того же кода, пока истинно какое-то условие. Если условие всегда истинно, то такой цикл называется бесконечным, у такого цикла нет точки выхода.

let userNumber = +prompt('enter you number');

if(userNumber < 5) {
    console.log('Sorry, no number');
} else {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 === 0 && i > 0) {
            console.log(i);
        }
    }
}